package co.edu.udistrital.curso.loops;

public class Examplefor {
	
	public static void main(String[] args) {
		System.out.println("Inicia Programa");
		
		for(int counter = 0; counter < 10 ; counter++) {
			System.out.println("counter -> " + counter);
			if(counter == 5)
				break;
		}
		
		System.out.println("Fin Programa");
	}
	
}
