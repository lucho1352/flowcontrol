package co.edu.udistrital.curso.loops;

public class ExampleforEach {
	
	public static void main(String[] args) {
		System.out.println("Inicia Programa");
		
		int score[] = {80, 91, 92, 68, 88};
		
		//for(int counter = 0; counter <= 4 ; counter++) {
		//	System.out.println(score[counter]);
		//}
		
		for(int value : score) {
			System.out.println(value);
		}
		
		System.out.println("Fin Programa");
	}
	
}
