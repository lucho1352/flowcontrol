package co.edu.udistrital.curso.flowcontrol;

public class ExampleSwitch2 {
	
	public static void main(String[] args) {
		System.out.println("Inicia Programa");
		
		int month = 1;
		int days = 0;
		
		switch(month) {
		case 2:
			days = 28;
			break;
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			days = 31;
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			days = 30;
			break;
			
		default:
			System.out.println("Invalid");
			break;
				
		}

		System.out.println("day => " + days);
		System.out.println("Fin Programa");
	}
	
}
