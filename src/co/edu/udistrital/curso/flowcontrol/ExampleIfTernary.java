package co.edu.udistrital.curso.flowcontrol;

public class ExampleIfTernary {
	
	public static void main(String[] args) {
		System.out.println("Inicia Programa");
		
		boolean isMoving = true;
		String output = "";
		
		output = isMoving ? "is Moving":"isn't Moving";
		
		System.out.println("output => " + output);
		System.out.println("Fin Programa");
	}
	
}
