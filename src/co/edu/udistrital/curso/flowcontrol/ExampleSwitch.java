package co.edu.udistrital.curso.flowcontrol;

public class ExampleSwitch {
	
	public static void main(String[] args) {
		System.out.println("Inicia Programa");
		
		int day = 4;
		String dayType = "";
		String dayText = "";
		
		switch(day) {
		case 1:
			dayType = "Mon";
		case 2:
			dayType = "Tue";
			break;
		case 3:
			dayType = "Wed";
			break;
		case 4:
			dayType = "Thr";
			break;
		case 5:
			dayType = "Fri";
			break;
		case 6:
			dayType = "Sat";
			break;
		case 7:
			dayType = "Sun";
			break;
		default:
			dayType = "Invalid";
			break;
				
		}

		System.out.println("dayText=> " + dayType);
		System.out.println("Fin Programa");
	}
	
}
