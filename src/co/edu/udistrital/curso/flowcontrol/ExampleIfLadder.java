package co.edu.udistrital.curso.flowcontrol;

public class ExampleIfLadder {
	
	public static void main(String[] args) {
		System.out.println("Inicia Programa");
		
		int testScore = 80;
		char grade = '0';
		
		if(testScore >= 90) {
			grade = 'A';
		}else if (testScore >= 80){
			grade = 'B';
		}else if (testScore >= 70){
			grade = 'C';
		}else if (testScore >= 60){
			grade = 'D';
		}else {
			grade = 'F';
		}
		
		System.out.println("Grade => " + grade);
		System.out.println("Fin Programa");
	}
	
}
