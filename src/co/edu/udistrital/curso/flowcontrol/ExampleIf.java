package co.edu.udistrital.curso.flowcontrol;

public class ExampleIf {
	
	public static void main(String[] args) {
		System.out.println("Inicia Programa");
		
		boolean isMoving = false;
		
		if(isMoving) {
			System.out.println("is Moving");
		}else {
			System.out.println("isn't Moving");
		}
		
		System.out.println("Fin Programa");
	}
	
}
